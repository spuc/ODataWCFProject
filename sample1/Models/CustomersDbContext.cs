﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace sample1.Models
{
    public class CustomersDbContext : DbContext
    {
        public CustomersDbContext()
            : base("NWConnection")
        {
        }

        public DbSet<Customer> customers { get; set; }

    }
}