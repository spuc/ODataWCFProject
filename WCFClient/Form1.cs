﻿using System;
using System.Data.Services.Client;
using System.Windows.Forms;
using WCFClient.ServiceReference1;

namespace WCFClient
{
    public partial class Form1 : Form
    {
        private northwindEntities1 _Proxy;
        DataServiceCollection<Customer> _Customers = new DataServiceCollection<Customer>();

         
        public Form1()
        {
            InitializeComponent();
            _Proxy = new northwindEntities1(new Uri("http://localhost:9614/NWDataService.svc"));
        }

        private void btnQuery_Click(object sender, System.EventArgs e)
        {
            _Customers.Load(_Proxy.Customers);
            dataGridView1.DataSource = _Customers;
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            _Proxy.SaveChanges();
        }
    }
}
