﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WCFDS.Startup))]
namespace WCFDS
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
