//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data.Services;
using System.Data.Services.Common;
using System.Data.Services.Providers;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel.Web;
using WCFDS.Models;

namespace WCFDS
{
    public class NWDataService : EntityFrameworkDataService<northwindEntities1>
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.
            // Examples:
            config.UseVerboseErrors = true;

            config.SetEntitySetAccessRule("Customers", EntitySetRights.All);  //can edit
            config.SetEntitySetAccessRule("Orders", EntitySetRights.AllRead);  //just read
            config.SetEntitySetAccessRule("Order_Details", EntitySetRights.AllRead);
            //config.SetServiceOperationAccessRule("OrderedCustomers", ServiceOperationRights.All);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }

        [WebGet(UriTemplate = "OrderedCustomers")]
        public IQueryable<Customer> OrderedCustomers()
        {
            return this.CurrentDataSource.Customers.OrderBy(c => c.CompanyName);
        }

        [QueryInterceptor("Customers")] //called when customers collection is queries..passes each customer and inspects each customer
        public Expression<Func<Customer, bool>> OnQueryCustomers()
        {
            return c => c.Country == "USA";  //method that inspects that customer and returns a boolean saying if it should be included in the results
        }

        [ChangeInterceptor("Customers")]
        public void UpdateCustomer(Customer c, UpdateOperations ops)  //indicated the operation..insert, update or delete
        {
            if (string.IsNullOrWhiteSpace(c.CompanyName))
                throw new DataServiceException("Company name is required-------.");
        }

        //security
        //[QueryInterceptor("SalesOrderHeaders")]
        //public Expression<Func<SalesOrderHeader, bool>> OnQuerySalesOrderHeaders()
        //{
        //    return (SalesOrderHeader p) => HttpContext.Current.User.IsInRole("Super Users");
        //}
        //---------------------------------------------------------------------------------------------
        //[ChangeInterceptor("SalesOrderHeaders")]
        //public void OnChangeSalesOrderHeaders(SalesOrderHeader header, UpdateOperations operation)
        //{
        //    if (operation == UpdateOperations.Add || operation == UpdateOperations.Change)
        //    {
        //        var user = HttpContext.Current.User;
        //        if (!user.IsInRole("Super Users"))
        //        {
        //            throw new DataServiceException(401,
        //                "The user does not have permission to add or change Sales Order Headers");
        //        }
        //    }
        //}



    }
}
